package com.zhaoj.savemoney.service;

import com.zhaoj.savemoney.ApplicationData;
import com.zhaoj.savemoney.database.AccountDBHelper;
import com.zhaoj.savemoney.database.AccountManagerDBHelper;
import com.zhaoj.savemoney.utils.PreferencesUtils;
import com.zhaoj.savemoney.utils.ConstantsUtils;

import android.app.IntentService;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.util.Log;

public class InitAppService extends IntentService {

	private final static String TAG = "InitAppService";
	private final static String QUERY_ACCOUNT_SQL_STRING = "select * from account_tbl where account_id = ?";

	public InitAppService() {
		super("InitAppService");
	}

	public InitAppService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		Log.v(TAG, "---> InitAppService");
		getCurVersionCode();
		InitPreferenceDate();
		InitSqlLiteDB();
	}

	private void InitPreferenceDate() {
		if (ApplicationData.preferencesData == null) {
			ApplicationData.preferencesData = new PreferencesUtils();
		}
		ApplicationData.preferencesData.InitAppDataFromPref(this);
	}

	private void getCurVersionCode() {
		PackageInfo info = null;
		ApplicationData.VersionCode = 1;
		try {
			info = this.getPackageManager().getPackageInfo(
					this.getPackageName(), 0);
			ApplicationData.VersionCode = info.versionCode;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
	}

	private void InitSqlLiteDB() {
		if (ApplicationData.accountDBHelper == null) {
			// 创建账本管理数据库
			ApplicationData.accountManagerDBHelper = new AccountManagerDBHelper(
					this,
					ConstantsUtils
							.getDBFileName(ConstantsUtils.ACCOUNT_MANAGER_DBNAME_STRING),
					null, ApplicationData.VersionCode);
			// 打开上次关闭时的账本
			Cursor cursor = ApplicationData.accountManagerDBHelper
					.getReadableDatabase().rawQuery(
							QUERY_ACCOUNT_SQL_STRING,
							new String[] { String
									.valueOf(ApplicationData.accountID) });
			if (cursor.getCount() != 0) {
				cursor.moveToFirst();
				ApplicationData.accountDBHelper = new AccountDBHelper(
						this,
						cursor.getString(cursor.getColumnIndex("account_path")),
						null, ApplicationData.VersionCode);
			} else {
				ApplicationData.accountDBHelper = new AccountDBHelper(
						this,
						ConstantsUtils
								.getDBFileName(ConstantsUtils.DEFAULT_DBNAME_STRING),
						null, ApplicationData.VersionCode);
				ApplicationData.accountID = 0;
			}
		}
	}
}
