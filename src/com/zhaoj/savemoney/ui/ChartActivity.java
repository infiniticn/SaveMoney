package com.zhaoj.savemoney.ui;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

import com.zhaoj.savemoney.ApplicationData;
import com.zhaoj.savemoney.R;
import com.zhaoj.savemoney.utils.ConstantsUtils;
import com.zhaoj.savemoney.widget.DateMenuBar;
import com.zhaoj.savemoney.widget.DateMenuBar.OnDateStringChangedListener;
import com.zhaoj.savemoney.widget.TopMenuNavbar;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.database.Cursor;
import android.graphics.Color;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RadioButton;

public class ChartActivity extends Activity implements OnCheckedChangeListener,
		OnDateStringChangedListener {
	private static final String DATEFORMAT_STRING = "yyyy年MM月";
	private static final String SHORT_DATEFORMAT_STRING = "yyyy-MM-dd";
	private static int[] COLORS = new int[] { Color.GREEN, Color.BLUE,
			Color.MAGENTA, Color.CYAN, Color.RED, Color.BLACK, Color.LTGRAY };
	public static long exitTime = 0;
	private static final String QUERY_CHARTITEM_SQL_STRING = "select costtype_tbl.costtype_name as name, "
			+ "costtype_tbl.costtype_icon_id as iconid, sum(default_account_tbl.defacu_amount) as amount "
			+ "from default_account_tbl, costtype_tbl "
			+ "where (default_account_tbl.defacu_kind = ?) and "
			+ "(default_account_tbl.defacu_costtype = costtype_tbl.costtype_id) and "
			+ "(datetime(default_account_tbl.defacu_time) between datetime(?, 'start of month') and datetime(?, 'start of month', '+1 month'))"
			+ "group by default_account_tbl.defacu_costtype "
			+ "order by default_account_tbl.defacu_costtype";
	private static final String QUERY_TOTALAMOUNT_SQL_STRING = "select sum(defacu_amount) as total, defacu_kind "
			+ "from default_account_tbl "
			+ "where (datetime(default_account_tbl.defacu_time) between datetime(?, 'start of month') and datetime(?, 'start of month', '+1 month'))"
			+ "group by defacu_kind " + "order by defacu_kind";
	private static final String QUERY_CHARTITEM_TOTAL_SQL_STRING = "select costtype_tbl.costtype_name as name, "
			+ "costtype_tbl.costtype_icon_id as iconid, sum(default_account_tbl.defacu_amount) as amount "
			+ "from default_account_tbl, costtype_tbl "
			+ "where (default_account_tbl.defacu_kind = ?) and "
			+ "(default_account_tbl.defacu_costtype = costtype_tbl.costtype_id) "
			+ "group by default_account_tbl.defacu_costtype "
			+ "order by default_account_tbl.defacu_costtype";
	private static final String QUERY_TOTALAMOUNT_TOTAL_SQL_STRING = "select sum(defacu_amount) as total, defacu_kind "
			+ "from default_account_tbl "
			+ "group by defacu_kind "
			+ "order by defacu_kind";

	private String mDateString;
	private boolean mIsTotal;

	private TopMenuNavbar mTopMenuNavbar;
	private DateMenuBar mDateMenuBar;

	private CategorySeries mSeries;
	private DefaultRenderer mRenderer;
	private GraphicalView mChartView;

	private RadioButton mOutButton;
	private RadioButton mInButton;
	private RadioButton mAllButton;
	private ListView mChartListView;

	private ChartAdapter mOutAdapter;
	private ChartAdapter mInAdapter;

	private double mOutTotalAmount;
	private double mInTotalAmount;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chart);

		mTopMenuNavbar = (TopMenuNavbar) findViewById(R.id.top_menu_navbar);
		mTopMenuNavbar.leftButton.setImageDrawable(getResources().getDrawable(
				R.drawable.icon_chart_small));
		mTopMenuNavbar.leftButton
				.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						mIsTotal = !mIsTotal;
						if (mIsTotal) {
							mTopMenuNavbar.leftButton
									.setImageDrawable(getResources()
											.getDrawable(R.drawable.icon_total));
						} else {
							mTopMenuNavbar.leftButton
									.setImageDrawable(getResources()
											.getDrawable(
													R.drawable.icon_chart_small));
						}
						refreshUI();
					}
				});
		mTopMenuNavbar.rightButton.setVisibility(View.INVISIBLE);
		mTopMenuNavbar.tvTitle.setText(R.string.title_activity_chart);

		mDateMenuBar = (DateMenuBar) findViewById(R.id.date_menu_bar);
		mDateMenuBar.setDateFormatString(DATEFORMAT_STRING);
		mDateMenuBar.leftButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mDateMenuBar.curCalendar.add(Calendar.MONTH, -1);
				mDateMenuBar.ReFreshDateFormat();

			}
		});

		mDateMenuBar.rightButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mDateMenuBar.curCalendar.add(Calendar.MONTH, 1);
				mDateMenuBar.ReFreshDateFormat();

			}
		});
		mDateMenuBar.setmOnDateChangedListener(this);

		mOutButton = (RadioButton) findViewById(R.id.radio_button_out);
		mOutButton.setOnCheckedChangeListener(this);
		mInButton = (RadioButton) findViewById(R.id.radio_button_in);
		mInButton.setOnCheckedChangeListener(this);
		mAllButton = (RadioButton) findViewById(R.id.radio_button_all);

		mChartListView = (ListView) findViewById(R.id.list_costtype_amount);

		getDateString();
		getTotalAmount();
		initChartAdapter();
		mChartListView.setAdapter(mOutAdapter);
		initialPieChartBuilder();
		refreshChart(mOutAdapter.CostTypePercent, mOutAdapter.CostTypeNames);
	}

	private void getTotalAmount() {
		Cursor cursor;
		if (mIsTotal) {
			cursor = ApplicationData.accountDBHelper.getReadableDatabase()
					.rawQuery(QUERY_TOTALAMOUNT_TOTAL_SQL_STRING,
							new String[] {});
		} else {
			cursor = ApplicationData.accountDBHelper.getReadableDatabase()
					.rawQuery(QUERY_TOTALAMOUNT_SQL_STRING,
							new String[] { mDateString, mDateString });
		}
		if (cursor.getCount() == 0) {
			mOutTotalAmount = 0.0;
			mInTotalAmount = 0.0;
			return;
		}

		while (cursor.moveToNext()) {
			if (cursor.getInt(cursor.getColumnIndex("defacu_kind")) == 0) {
				mOutTotalAmount = cursor.getDouble(cursor
						.getColumnIndex("total"));
			} else if (cursor.getInt(cursor.getColumnIndex("defacu_kind")) == 1) {
				mInTotalAmount = cursor.getDouble(cursor
						.getColumnIndex("total"));
			}
		}
	}

	@Override
	protected void onResume() {
		refreshUI();
		super.onResume();
	}

	private void refreshButtonText() {
		if (mIsTotal) {
			mOutButton.setText("总支出:￥" + Double.toString(mOutTotalAmount));
			mInButton.setText("总收入:￥" + Double.toString(mInTotalAmount));
			mAllButton.setText("总余额:￥"
					+ Double.toString(mInTotalAmount + mOutTotalAmount));
		} else {
			mOutButton.setText("支出：￥" + Double.toString(mOutTotalAmount));
			mInButton.setText("收入：￥" + Double.toString(mInTotalAmount));
			mAllButton.setText("余额：￥"
					+ Double.toString(mInTotalAmount + mOutTotalAmount));
		}
	}

	private void refreshAdapter() {
		mOutAdapter.refresh(ConstantsUtils.COST_KIND_OUT, mDateString);
		mInAdapter.refresh(ConstantsUtils.COST_KIND_IN, mDateString);
	}

	@SuppressLint("SimpleDateFormat")
	private void refreshUI() {
		getDateString();
		getTotalAmount();
		refreshButtonText();
		refreshAdapter();
		if (mOutButton.isChecked()) {
			refreshChart(mOutAdapter.CostTypePercent, mOutAdapter.CostTypeNames);
		} else if (mInButton.isChecked()) {
			refreshChart(mInAdapter.CostTypePercent, mInAdapter.CostTypeNames);
		}
	}

	@SuppressLint("SimpleDateFormat")
	private void getDateString() {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				SHORT_DATEFORMAT_STRING);
		mDateString = dateFormat.format(mDateMenuBar.curCalendar.getTime());
	}

	private void initChartAdapter() {
		mOutAdapter = new ChartAdapter(ConstantsUtils.COST_KIND_OUT,
				mDateString);
		mInAdapter = new ChartAdapter(ConstantsUtils.COST_KIND_IN,
				mDateString);
	}

	private void initialPieChartBuilder() {

		mRenderer = new DefaultRenderer();
		mRenderer.setApplyBackgroundColor(true);
		mRenderer.setBackgroundColor(Color.TRANSPARENT);
		// mRenderer.setChartTitleTextSize(20);

		mRenderer.setLabelsTextSize(20);
		mRenderer.setLabelsColor(Color.BLACK);
		mRenderer.setShowAxes(false); // 是否显示轴线
		// mRenderer.setAxesColor(Color.RED); //设置轴颜色

		mRenderer.setFitLegend(false);
		mRenderer.setInScroll(false);
		mRenderer.setPanEnabled(false);
		mRenderer.setShowCustomTextGrid(false);
		mRenderer.setShowLegend(false); // 不显示图例
		mRenderer.setShowGrid(false);
		mRenderer.setClickEnabled(true);

		mRenderer.setScale(1.5f);
		mRenderer.setLegendTextSize(15);
		mRenderer.setMargins(new int[] { 20, 50, 15, 0 });
		mRenderer.setZoomButtonsVisible(true);

		mRenderer.setStartAngle(45);
		mSeries = new CategorySeries("");

		LinearLayout layout = (LinearLayout) findViewById(R.id.layout_chart);
		mChartView = ChartFactory.getPieChartView(this, mSeries, mRenderer);
		layout.addView(mChartView, new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));
	}

	private void refreshChart(double percent[], String category[]) {
		mSeries.clear(); // 清空类别序列
		for (SimpleSeriesRenderer renderer : mRenderer.getSeriesRenderers()) {
			// 移除老的图表列渲染器
			mRenderer.removeSeriesRenderer(renderer);
		}

		for (int i = 0; i < percent.length; i++) {
			if (percent[i] > 0) {
				// 添加一个图表列，第一个参数是标签，第二个参数是百分比，CATEGORY和COLORS为自定义数组
				mSeries.add(category[i] + ":" + String.valueOf(percent[i]),
						percent[i]);
				SimpleSeriesRenderer renderer = new SimpleSeriesRenderer();
				renderer.setColor(COLORS[i % COLORS.length]);
				// 为图表列添加渲染器
				mRenderer.addSeriesRenderer(renderer);
			}
		}

		if (percent.length == 0) {
			mSeries.add("", 1);
			SimpleSeriesRenderer renderer = new SimpleSeriesRenderer();
			renderer.setColor(COLORS[1 % COLORS.length]);
			// 为图表列添加渲染器
			mRenderer.addSeriesRenderer(renderer);
		}

		if (mChartView != null) {
			// 重绘
			mChartView.repaint();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.chart, menu);
		return true;
	}

	@SuppressLint("NewApi")
	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (isChecked) {
			buttonView.setBackground(getResources().getDrawable(
					R.drawable.chart_radiobutton_selected));
			switch (buttonView.getId()) {
			case R.id.radio_button_out:
				mChartListView.setAdapter(mOutAdapter);
				refreshChart(mOutAdapter.CostTypePercent,
						mOutAdapter.CostTypeNames);
				break;
			case R.id.radio_button_in:
				mChartListView.setAdapter(mInAdapter);
				refreshChart(mInAdapter.CostTypePercent,
						mInAdapter.CostTypeNames);
				break;
			}
		} else {
			buttonView.setBackground(getResources().getDrawable(
					R.drawable.chart_radiobutton_background));
		}

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			exit();
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	public  void exit() {
		if ((System.currentTimeMillis() - exitTime) > 20000) {
			Toast.makeText(
					ApplicationData.MainActivitycontext.getApplicationContext(),
					"再按一次退出程序", Toast.LENGTH_SHORT).show();
			exitTime = System.currentTimeMillis();
		} else {
			finish();
			System.exit(0);
		}
	}

	public class ChartAdapter extends BaseAdapter {
		private int CostTypeICONIDs[];
		public String CostTypeNames[];
		private double CostTypeAmount[];
		public double CostTypePercent[];

		private String mDateString;

		@SuppressLint("Recycle")
		public ChartAdapter(int costKind, String dateString) {
			mDateString = dateString;
			InitChartAdapterData(costKind);
		}

		public void InitChartAdapterData(int costKind) {
			Cursor cursor;
			if (mIsTotal) {
				cursor = ApplicationData.accountDBHelper.getReadableDatabase()
						.rawQuery(QUERY_CHARTITEM_TOTAL_SQL_STRING,
								new String[] { Integer.toString(costKind), });
			} else {
				cursor = ApplicationData.accountDBHelper.getReadableDatabase()
						.rawQuery(
								QUERY_CHARTITEM_SQL_STRING,
								new String[] { Integer.toString(costKind),
										mDateString, mDateString });
			}
			CostTypeICONIDs = new int[cursor.getCount()];
			CostTypeNames = new String[cursor.getCount()];
			CostTypeAmount = new double[cursor.getCount()];
			CostTypePercent = new double[cursor.getCount()];

			int i = 0;
			while (cursor.moveToNext()) {
				CostTypeNames[i] = cursor.getString(cursor
						.getColumnIndex("name"));
				CostTypeICONIDs[i] = cursor.getInt(cursor
						.getColumnIndex("iconid"));
				CostTypeAmount[i] = cursor.getDouble(cursor
						.getColumnIndex("amount"));
				if (costKind == 0) {
					if (mOutTotalAmount == 0) {
						CostTypePercent[i] = 1;
					} else {
						CostTypePercent[i] = (double) (Math
								.round((CostTypeAmount[i] / mOutTotalAmount * 100) * 100) / 100.0);
					}
				} else {
					if (mInTotalAmount != 0) {
						CostTypePercent[i] = (double) (Math
								.round((CostTypeAmount[i] / mInTotalAmount * 100) * 100) / 100.0);
					} else {
						CostTypePercent[i] = 1;
					}

				}
				i++;
			}
		}

		@Override
		public int getCount() {
			return CostTypeNames.length;
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;

			if (convertView == null) {
				holder = new ViewHolder();
				convertView = View.inflate(ChartActivity.this,
						R.layout.layout_chartitem, null);
				holder.icon = (ImageView) convertView
						.findViewById(R.id.costtype_icon);
				holder.name = (TextView) convertView
						.findViewById(R.id.costtype_name);
				holder.amount = (TextView) convertView
						.findViewById(R.id.costtype_amount);
				holder.percent = (TextView) convertView
						.findViewById(R.id.costtype_percent);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			holder.icon.setImageDrawable(getResources().getDrawable(
					CostTypeICONIDs[position]));
			holder.name.setText(CostTypeNames[position]);
			holder.amount.setText("￥"
					+ String.valueOf(CostTypeAmount[position]));
			holder.percent.setTextColor(COLORS[position % COLORS.length]);
			holder.percent.setText(String.valueOf(CostTypePercent[position])
					+ "%");
			return convertView;
		}

		public void refresh(int costKind, String dateString) {
			mDateString = dateString;
			InitChartAdapterData(costKind);
			notifyDataSetChanged();
			notifyDataSetInvalidated();
		}

		class ViewHolder {
			private ImageView icon;
			private TextView name;
			private TextView amount;
			private TextView percent;
		}
	}

	@Override
	public void onDateStringChanged() {
		refreshUI();
	}

}
