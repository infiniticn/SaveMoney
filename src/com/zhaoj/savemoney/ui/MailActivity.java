package com.zhaoj.savemoney.ui;

import com.zhaoj.savemoney.ApplicationData;
import com.zhaoj.savemoney.R;
import com.zhaoj.savemoney.widget.TopMenuNavbar;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MailActivity extends Activity {
	private EditText contenText;
	private Button sendButton;
	private TopMenuNavbar mTopMenuNavbar;
	public static long exitTime = 0;
	private String contentString;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mail);
		mTopMenuNavbar = (TopMenuNavbar) findViewById(R.id.mail_top_menu_bar);
		mTopMenuNavbar.tvTitle.setText(R.string.title_activity_mail);
		mTopMenuNavbar.leftButton.setVisibility(View.GONE);
		mTopMenuNavbar.rightButton.setVisibility(View.GONE);
		
		contenText = (EditText) findViewById(R.id.mail_content);

		sendButton = (Button) findViewById(R.id.mail_send_button);
		sendButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent sendMailIntent = new Intent()
						.setAction(Intent.ACTION_SENDTO);
				sendMailIntent.setData(Uri.parse("mailto:zj2012zy@163.com"));
				sendMailIntent.putExtra(Intent.EXTRA_SUBJECT,
						"SaveMoney APP 问题反馈");
				contentString = contenText.getText().toString();
				sendMailIntent.putExtra(Intent.EXTRA_TEXT, contentString);
				startActivity(sendMailIntent);
			}
		});
	}

	@Override
	protected void onResume() {
		contenText.setText(null);
		super.onResume();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			exit();
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	public void exit() {
		if ((System.currentTimeMillis() - exitTime) > 20000) {
			Toast.makeText(
					ApplicationData.MainActivitycontext.getApplicationContext(),
					"再按一次退出程序", Toast.LENGTH_SHORT).show();
			exitTime = System.currentTimeMillis();
		} else {
			finish();
			System.exit(0);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.mail, menu);
		return true;
	}

}
