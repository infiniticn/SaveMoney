package com.zhaoj.savemoney.widget;

public class AccountItem {

	private String mAccountName;
	private String mAccountPath;
	private boolean mIsShare;
	private int mAccountID;

	public AccountItem() {
		// TODO Auto-generated constructor stub
	}

	public AccountItem(String acccountName, String accountPath, int isShare, int accountID) {
		mAccountName = acccountName;
		mAccountPath = accountPath;
		mIsShare = !(isShare == 0);
		mAccountID = accountID;
	}

	public String getmAccountName() {
		return mAccountName;
	}

	public void setmAccountName(String mAccountName) {
		this.mAccountName = mAccountName;
	}

	public String getmAccountPath() {
		return mAccountPath;
	}

	public void setmAccountPath(String mAccountPath) {
		this.mAccountPath = mAccountPath;
	}

	public boolean getmIsShare() {
		return mIsShare;
	}

	public void setmIsShare(boolean mIsShare) {
		this.mIsShare = mIsShare;
	}

	public int getmAccountID() {
		return mAccountID;
	}

	public void setmAccountID(int mAccountID) {
		this.mAccountID = mAccountID;
	}
}
